package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.ICommandRepository;
import ru.tsk.vkorenygin.tm.constant.ArgumentConst;
import ru.tsk.vkorenygin.tm.constant.TerminalConst;
import ru.tsk.vkorenygin.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "displays developer info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "displays program version"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "displays list of commands"
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "displays system information"
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "displays available commands"
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "displays available launch arguments"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "closes the application"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "displays task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "removes all tasks."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "creates new task."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null, "shows task by it's id."
    );

    private static final Command TASK_SHOW_BY_NAME = new Command(
            TerminalConst.TASK_SHOW_BY_NAME, null, "shows task by it's name."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null, "shows task by it's index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "removes task by it's id."
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "removes task by it's name."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "removes task by it's index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "updates task by it's id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "updates task by it's index."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "displays project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "removes all projects."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "creates new project."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null, "shows project by it's id."
    );

    private static final Command PROJECT_SHOW_BY_NAME = new Command(
            TerminalConst.PROJECT_SHOW_BY_NAME, null, "shows project by it's name."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null, "shows project by it's index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "removes project by it's id."
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "removes project by it's name."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "removes project by it's index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "updates project by it's id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "updates project by it's index."
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            COMMANDS, ARGUMENTS, HELP, INFO, VERSION, ABOUT,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_NAME, TASK_SHOW_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            PROJECT_LIST, PROJECT_CLEAR, PROJECT_CREATE,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_NAME, PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_NAME,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
