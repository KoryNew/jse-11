package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public boolean existsById(String id) {
        final Task task = findById(id);
        return task != null;
    }

    @Override
    public boolean existsByIndex(int index) {
        if (index < 0)
            return false;
        return index < tasks.size();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task findById(final String id) {
        for (Task task: tasks) {
            if (id.equals(task.getId()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task: tasks) {
            if (name.equals(task.getName()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final int index) {
        return tasks.get(index); //
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
