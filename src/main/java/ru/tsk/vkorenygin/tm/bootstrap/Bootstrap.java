package ru.tsk.vkorenygin.tm.bootstrap;

import ru.tsk.vkorenygin.tm.api.controller.ICommandController;
import ru.tsk.vkorenygin.tm.api.controller.IProjectController;
import ru.tsk.vkorenygin.tm.api.controller.ITaskController;
import ru.tsk.vkorenygin.tm.api.repository.ICommandRepository;
import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.ICommandService;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.api.service.ITaskService;
import ru.tsk.vkorenygin.tm.constant.ArgumentConst;
import ru.tsk.vkorenygin.tm.constant.TerminalConst;
import ru.tsk.vkorenygin.tm.controller.CommandController;
import ru.tsk.vkorenygin.tm.controller.ProjectController;
import ru.tsk.vkorenygin.tm.controller.TaskController;
import ru.tsk.vkorenygin.tm.repository.CommandRepository;
import ru.tsk.vkorenygin.tm.repository.ProjectRepository;
import ru.tsk.vkorenygin.tm.repository.TaskRepository;
import ru.tsk.vkorenygin.tm.service.CommandService;
import ru.tsk.vkorenygin.tm.service.ProjectService;
import ru.tsk.vkorenygin.tm.service.TaskService;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);

    public void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0)
            return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args))
            System.exit(0);
        while (true)
            parseCommand(TerminalUtil.nextLine());
    }

    public void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty())
            return;
        switch (arg) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showAll();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case TerminalConst.TASK_SHOW_BY_NAME:
                taskController.showByName();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showAll();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_NAME:
                projectController.showByName();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clear();
                break;
            default:
                commandController.showErrorCommand();
        }
    }
}
