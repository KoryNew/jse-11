package ru.tsk.vkorenygin.tm.controller;

import jdk.nashorn.internal.ir.Terminal;
import ru.tsk.vkorenygin.tm.api.controller.ITaskController;
import ru.tsk.vkorenygin.tm.api.service.ITaskService;
import ru.tsk.vkorenygin.tm.model.Task;
import ru.tsk.vkorenygin.tm.service.TaskService;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void create() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showAll() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task: tasks)
            System.out.println(task);
        System.out.println("[OK]");
    }

    public void show(Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
    }

    @Override
    public void showById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(task);
    }

    @Override
    public void showByName(){
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(task);
    }

    @Override
    public void showByIndex(){
        System.out.println("Enter index");
        final int index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(task);
    }

    @Override
    public void updateByIndex() {

        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index)) {
            System.out.println("[Incorrect value]");
            return;
        }

        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();

        if (taskService.updateByIndex(index, name, description) == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Updated task]");
    }

    @Override
    public void updateById() {

        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id)) {
            System.out.println("[Incorrect value]");
            return;
        }

        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();

        if (taskService.updateById(id, name, description) == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Updated task]");

    }

    @Override
    public void removeById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Task removed]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Task removed]");
    }

    @Override
    public void removeByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Task removed]");
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
