package ru.tsk.vkorenygin.tm.controller;

import ru.tsk.vkorenygin.tm.api.controller.IProjectController;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void create() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showAll() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        for (Project project: projects)
            System.out.println(project);
        System.out.println("[OK]");
    }

    public void show(Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
    }

    @Override
    public void showById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(project);
    }

    @Override
    public void showByName(){
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(project);
    }

    @Override
    public void showByIndex(){
        System.out.println("Enter index");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("Incorrect value.");
            return;
        }
        show(project);
    }

    @Override
    public void updateByIndex() {

        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!projectService.existsByIndex(index)) {
            System.out.println("[Incorrect value]");
            return;
        }

        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();

        if (projectService.updateByIndex(index, name, description) == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Updated project]");
    }

    @Override
    public void updateById() {

        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        if (!projectService.existsById(id)) {
            System.out.println("[Incorrect value]");
            return;
        }

        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();

        if (projectService.updateById(id, name, description) == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Updated project]");
    }

    @Override
    public void removeById() {
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Project removed]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Project removed]");
    }

    @Override
    public void removeByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null)
            System.out.println("[Incorrect value]");
        else
            System.out.println("[Project removed]");
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
