package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task Task);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    List<Task> findAll();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    void remove(Task Task);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    void clear();

}
