package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    List<Project> findAll();

    Project findById(final String id);

    Project findByName(final String name);

    Project findByIndex(final int index);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final int index, final String name, final String description);

    void remove(Project project);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);

    void clear();

}
