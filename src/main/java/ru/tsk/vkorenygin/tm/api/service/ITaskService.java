package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    List<Task> findAll();

    Task findById(String Id);

    Task findByName(String name);

    Task findByIndex(int index);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final int index, final String name, final String description);

    void remove(Task task);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    void clear();

}
