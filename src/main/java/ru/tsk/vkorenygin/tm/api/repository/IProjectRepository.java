package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    List<Project> findAll();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);

    void clear();

}
