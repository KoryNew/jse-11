package ru.tsk.vkorenygin.tm.api.controller;

import ru.tsk.vkorenygin.tm.model.Task;

public interface ITaskController {

    void create();

    void show(final Task task);

    void showById();

    void showByIndex();

    void showByName();

    void updateById();

    void updateByIndex();

    void showAll();

    void removeById();

    void removeByIndex();

    void removeByName();

    void clear();
}
