package ru.tsk.vkorenygin.tm.api.controller;

import ru.tsk.vkorenygin.tm.model.Project;

public interface IProjectController {

    void create();

    void show(final Project project);

    void showById();

    void showByIndex();

    void showByName();

    void updateById();

    void updateByIndex();

    void showAll();

    void removeById();

    void removeByIndex();

    void removeByName();

    void clear();

}
