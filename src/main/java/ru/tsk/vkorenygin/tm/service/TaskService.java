package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.ITaskService;
import ru.tsk.vkorenygin.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String name) {
        if (name == null || name.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(String name, String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty())
            return false;
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final int index) {
        return taskRepository.existsByIndex(index);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findByIndex(final int index) {
        if (index < 0)
            return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty())
            return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task updateByIndex(final int index, final String name, final String description) {
        if (index < 0)
            return null;
        if (name == null || name.isEmpty())
            return null;
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty())
            return null;
        if (name == null || name.isEmpty())
            return null;
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void remove(Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty())
            return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final int index) {
        if (index < 0)
            return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
